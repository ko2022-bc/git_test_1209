package com.example.demo.domain;

import org.hibernate.validator.constraints.Length;

import jakarta.validation.constraints.NotBlank;
import lombok.Data;

@Data
public class AccountDto {
	
	private int userId;

	@NotBlank
	@Length(max=5)
	private String userName;
}
