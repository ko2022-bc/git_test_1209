package com.example.demo.domain;

import java.util.ArrayList;
import java.util.List;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import lombok.Data;

@Data
public class ResponseDto {

	private boolean status = true;
	private String code = "";
	private String message = "";
	private List<ValidationDto> validation = null;
	private List<?> list = null;
	
	public void setValidation(BindingResult result) {
		List<ValidationDto> list = new ArrayList<ValidationDto>();
		for (FieldError error : result.getFieldErrors()) {
			ValidationDto rec = new ValidationDto();
			rec.setCode(error.getCode());
			rec.setField(error.getField());
			rec.setMessage(error.getDefaultMessage());
			list.add(rec);
			status = false;
		}
		validation = list;
	}
}
