package com.example.demo.domain;

import lombok.Data;

@Data
public class ValidationDto {
	private String code = "";
	private String field = "";
	private String message = "";
}
