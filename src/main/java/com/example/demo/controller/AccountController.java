package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.AccountDto;
import com.example.demo.domain.ResponseDto;
import com.example.demo.service.AccountService;

@RestController
@RequestMapping("/api/account")
public class AccountController {
	@Autowired
    private AccountService accountService;

    @RequestMapping(value="/insert", method=RequestMethod.POST)
    public ResponseDto insert(@RequestBody @Validated AccountDto param, BindingResult result) {
    	ResponseDto response = new ResponseDto();
    	if (result.hasErrors()) {
    		response.setValidation(result);
    		return response;
    	}
    	accountService.insert(param);
        return response;
    }

    @RequestMapping(value="/update", method=RequestMethod.POST)
    public ResponseDto update(@RequestBody AccountDto param, BindingResult result) {
    	ResponseDto response = new ResponseDto();
    	if (result.hasErrors()) {
    		response.setValidation(result);
    		return response;
    	}
    	accountService.update(param);
        return response;
    }

    @RequestMapping(value="/delete", method=RequestMethod.POST)
    public ResponseDto delete(@RequestBody AccountDto param, BindingResult result) {
    	ResponseDto response = new ResponseDto();
    	if (result.hasErrors()) {
    		response.setValidation(result);
    		return response;
    	}
    	accountService.delete(param.getUserId());
        return response;
    }

    @RequestMapping(value="/select", method=RequestMethod.POST)
    public ResponseDto select() {
    	ResponseDto result = new ResponseDto();
    	result.setList(accountService.select());
        return result;
    }
}
