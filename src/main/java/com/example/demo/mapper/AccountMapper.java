package com.example.demo.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.example.demo.domain.AccountDto;

@Mapper
public interface AccountMapper {
    List<AccountDto> select();

    AccountDto selectByPrimary(int id);

    int insert(AccountDto account);

    int update(AccountDto account);

    int delete(int id);
}
