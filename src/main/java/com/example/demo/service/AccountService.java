package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.demo.domain.AccountDto;
import com.example.demo.mapper.AccountMapper;

@Service
public class AccountService {
    @Autowired
    private AccountMapper accountMapper;

    @Transactional
    public List<AccountDto> select() {
        return accountMapper.select();
    }

    @Transactional
    public AccountDto selectByPrimary(int id) {
        return accountMapper.selectByPrimary(id);
    }

    @Transactional
    public int insert(AccountDto account) {
        return accountMapper.insert(account);
    }

    @Transactional
    public int update(AccountDto account) {
        return accountMapper.update(account);
    }

    @Transactional
    public int delete(int id) {
        return accountMapper.delete(id);
    }
}
